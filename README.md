# README #

This is the Arduino library for my YAMC (Yet Another Minecraft Cube) project. 

Instructions on printing, constructing, and programming the cube can be found on my blog: [Tripping the Nerd Curmudgeonly](http://trippingthenerdcurmudgeonly.blogspot.com/)

## SUMMARY ##

The example sketch contains 3 modes for the Cube. To change the mode, 
  press the rotary encoder, and the cube will blink white 1, 2, or 3
  times depending on the mode its entering.

MODE_SHIFT - this mode fades between each color defined in the color seequence.
  Turning the encoder will speed up or slow down the speed of the color
  shifting. 

MODE_TWINKLE - this mode displays a solid color from the color sequence,
  subtly increasing/decreasing the color of each pixel, causing a 
  twinkily effect. Turning the enocder lets you select a different color.

MODE_RAVE - get out your glow stick! This mode choses a random color and 
  flashes that color on a random pixel, picks another random color, flashes
  it on a pixel, wash, rinse, repeat. Turn the encoder to speed up or
  slow down the speed of the flashes.

Special thanks to Adafruit for all their awesome sample code and products - 
  without which I would have never built this!

Also special thanks to Ben Buxton for his Rotary Encoder library which 
  I've modified for use with this example sketch and library. For his 
  original sketch please visit 
  [http://www.buxtronix.net/2011/10/rotary-encoders-done-properly.html](http://www.buxtronix.net/2011/10/rotary-encoders-done-properly.html)

## OTHER STUFF YOU'LL NEED ##

* [.STL files for the Cube on Thingiverse](http://www.thingiverse.com)
* [Rotary_Button Library](https://bitbucket.org/Dershum/rotary_button/overview)
* [Adafruit NeoPxiel Library](https://github.com/adafruit/Adafruit_NeoPixel)