class YAMC_Matrix {
	public:
		//YAMC_Matrix(int num_pixels, int data_pin);				// Default constructor
		YAMC_Matrix(int size, int data_pin);   						// Initialize the matrix to a given size
		void setPixel(YAMC_RGB rgb, int index);     		   		// Sets the RGB value of a specific pixel
		void setPixel(int red, int green, int blue, int index);		// Sets the RGB value of a specific pixel
			
		void setMatrix(YAMC_RGB rgb);                    			// Sets all pixels in the matrix to a given RGB value
		void setMatrix(int red, int green, int blue); 
		void saveMatrix();											// Saves the current matrix to the backup matrix
		void restoreMatrix();										// Restores the backup matrix to the curent matrix
		void shiftStep(YAMC_RGB start_rgb, 							// Shifts the current matrix by one step from the given RGB towards the target RGB
			YAMC_RGB end_rgb, int current_step, int num_steps);
		int calcShiftValue(int start_value, 						// Calculates the value to shift a partiucar R,G, or B value to
			int end_value, int current_step, int num_steps);
		void getPixel(int index, YAMC_RGB &rgb);  	   				// Returns a pointer to a pixel at the given index
		YAMC_RGB getPixel(int index);
		void paintMatrix();										    // Paints the matrix
		void setBrightness(int brightness);							// Sets the brightness of the pixels
		void setBrightness(int brightness, int index);				// Sets the brightness of an individual pixel
		void begin();												// Wrapper to start the neopixels, etc

	private:
		int _size;
		YAMC_RGB *_matrix;
		YAMC_RGB *_tempMatrix;
		static Adafruit_NeoPixel *_neo_pixels;
};
