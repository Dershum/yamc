#ifndef YAMC_Matrix_h 
#define YAMC_Matrix_h

#include "Arduino.h"
#include "Adafruit_NeoPixel.h"
#include "YAMC_RGB.h"
#include "YAMC_Matrix.h"

Adafruit_NeoPixel *YAMC_Matrix::_neo_pixels;

YAMC_Matrix::YAMC_Matrix(int size, int data_pin) {
	_neo_pixels = new Adafruit_NeoPixel(size, data_pin, NEO_RGB + NEO_KHZ800);
	_size = size;
	_matrix = new YAMC_RGB[_size];

	for (int i = 0; i < _size; i++) {
		_matrix[i] = YAMC_RGB(0, 0, 0);
	}
}

void YAMC_Matrix::setPixel(int red, int green, int blue, int index) {
	if (index < _size) {
		_matrix[index].setRed(red);
		_matrix[index].setGreen(green);
		_matrix[index].setBlue(blue);
	}
}

void YAMC_Matrix::setPixel(YAMC_RGB rgb, int index) {
	setPixel(rgb.red(), rgb.green(), rgb.blue(), index);
}

void YAMC_Matrix::setMatrix(int red, int green, int blue) {
	for (int i = 0; i < _size; i++) {
		setPixel(red, green, blue, i);
	}
}

void YAMC_Matrix::setMatrix(YAMC_RGB rgb) {
	setMatrix(rgb.red(), rgb.green(), rgb.blue());
}

void YAMC_Matrix::saveMatrix() {
	_tempMatrix = new YAMC_RGB[_size];

	for (int i = 0; i < _size; i++) {
		_tempMatrix[i] = YAMC_RGB(_matrix[i].red(), _matrix[i].green(), _matrix[i].blue());
	}
}

void YAMC_Matrix::restoreMatrix() {
	for (int i = 0; i < _size; i++) {
		setPixel(_tempMatrix[i].red(), _tempMatrix[i].green(), _tempMatrix[i].blue(), i);
	}

	delete[] _tempMatrix;
}

void YAMC_Matrix::shiftStep(YAMC_RGB start_rgb, YAMC_RGB end_rgb, 
	int current_step, int num_steps) {

	setMatrix(
		calcShiftValue(start_rgb.red(), end_rgb.red(), current_step, num_steps), 
		calcShiftValue(start_rgb.green(), end_rgb.green(), current_step, num_steps),
		calcShiftValue(start_rgb.blue(), end_rgb.blue(), current_step, num_steps)
	);
}

int YAMC_Matrix::calcShiftValue(int start_value, int end_value, 
	int current_step, int num_steps) {
	return start_value - (((start_value - end_value) / num_steps) * current_step);
}

void YAMC_Matrix::getPixel(int index, YAMC_RGB &rgb) {
	rgb.setRed(_matrix[index].red());
	rgb.setGreen(_matrix[index].green());
	rgb.setBlue(_matrix[index].blue());
}

YAMC_RGB YAMC_Matrix::getPixel(int index) {
	return _matrix[index];
}

void YAMC_Matrix::paintMatrix() {
	// Set the color of the pixel
	for (int i = 0; i < _size; i++) {
		YAMC_RGB pixel = getPixel(i);
		_neo_pixels->setPixelColor(i, _neo_pixels->Color(
			pixel.brightness() * pixel.red() / 255, 
			pixel.brightness() * pixel.green() / 255, 
			pixel.brightness() * pixel.blue() / 255
		));
	}

	// Paint the colors onto the neo pixel matrix
	_neo_pixels->show();
}

void YAMC_Matrix::setBrightness(int brightness, int pixel) {
	if (brightness > 255) {
		brightness = 255;
	}
	else if (brightness < 0) {
		brightness = 0;
	}

	_matrix[pixel].setBrightness(brightness);

}

void YAMC_Matrix::setBrightness(int brightness) {
	if (brightness > 255) {
		brightness = 255;
	}
	else if (brightness < 0) {
		brightness = 0;
	}

	for (int i = 0; i < _size; i++) {
		_matrix[i].setBrightness(brightness);
	}
}

void YAMC_Matrix::begin() {
	_neo_pixels->begin();
	_neo_pixels->setBrightness(255);
}

#endif