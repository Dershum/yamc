#ifndef YAMC_Timer_h 
#define YAMC_Timer_h

#include "Arduino.h"
#include "YAMC_Timer.h"

YAMC_Timer::YAMC_Timer() {
    _length = 0;
    _timerStart = 0;
    _timerStop = 0;
    _isRunning = false;
}

YAMC_Timer::YAMC_Timer(unsigned long length) {
    _length = length;
    _timerStart = 0;
    _timerStop = 0;
    _isRunning = false;
}

void  YAMC_Timer::startTimer() {
    _timerStart = millis();
    _isRunning = true;
    }

void YAMC_Timer::startTimer(unsigned long length) {
    _length = length;
    startTimer();
}

void YAMC_Timer::stopTimer() {
    _timerStop = millis();
    _isRunning = false;
}

void YAMC_Timer::restartTimer() {
    _timerStart = _timerStop - _timerStart + millis();
    _isRunning = true;
}

unsigned long YAMC_Timer::getTimerStart() {
    return _timerStart;
}

unsigned long YAMC_Timer::getTimerStop() {
    return _timerStop;
}

bool YAMC_Timer::isExpired() {
    if (millis() > _timerStart + _length) {
        stopTimer();
        return true;
    }
    else {
        return false;
    }
}

void YAMC_Timer::setLength(unsigned long length) {
    _length = length;
}


#endif

