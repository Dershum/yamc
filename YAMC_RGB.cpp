#ifndef YAMC_RBG_h 
#define YAMC_RGB_h

#include "Arduino.h"
#include "YAMC_RGB.h"

YAMC_RGB::YAMC_RGB() {
	_red = 0;
	_green = 0;
	_blue = 0;
	_brightness = 255;
}

YAMC_RGB::YAMC_RGB(int red, int green, int blue) {
	_red = red;
	_green = green;
	_blue = blue;
	_brightness = 255;
}

YAMC_RGB::YAMC_RGB(long hex) {
	_red = (hex >> 16);
	_green = (hex >> 8) & 0xFF;
	_blue = hex & 0xFF;
	_brightness = 255;
}

long YAMC_RGB::getColor() {
	return ((long)_red << 16) | ((long)_green << 8) | (long)_blue;
}

int YAMC_RGB::red() {
	return _red;
}

int YAMC_RGB::green() {
	return _green;
}

int YAMC_RGB::blue() {
	return _blue;
}

int YAMC_RGB::brightness() {
	return _brightness;
}

void YAMC_RGB::setRed(int red) {
	_red = red;
}

void YAMC_RGB::setGreen(int green) {
	_green = green;
}

void YAMC_RGB::setBlue(int blue) {
	_blue = blue;
}

void YAMC_RGB::setBrightness(int brightness) {
	_brightness = brightness;
}

int YAMC_RGB::calcMaxDiff(YAMC_RGB in_rgb) {
	int rtn_max = abs(_red - in_rgb.red());

	if (abs(_green - in_rgb.green()) > rtn_max) {
		rtn_max = abs(_green - in_rgb.green());
	}

	if (abs(_blue - in_rgb.blue()) > rtn_max) {
		rtn_max = abs(_blue - in_rgb.blue());
	}

	return rtn_max;
}

bool YAMC_RGB::equals(YAMC_RGB in_rgb) {
	if (_green == in_rgb.green() &&
		_red == in_rgb.red() &&
		_blue == in_rgb.blue()) {
		// Doesn't compare the intensity (should it?)
		return true;
	}
	else {
		return false;
	}
}

#endif