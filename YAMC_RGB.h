class YAMC_RGB {
	public:
		YAMC_RGB();
		YAMC_RGB(int r, int g, int b);					// Create an RGB object with provided values
		YAMC_RGB(int r, int g, int b, int i);			// Create an RGB object with brightness (intensity) value
		YAMC_RGB(long hex);								// Create an RGB object from a HEX color value 0xRRGGBB
		int calcMaxDiff(YAMC_RGB in_rgb);				// Calculate max difference between colors in the object
		int red();										// Gets the RED value
		int green();									// Gets the GREEN value
		int blue();										// Gets the BLUE value
		int brightness();								// Gets the brigntess of a pixel
		void setRed(int red);							// Sets the RED value
		void setGreen(int green);						// Sets the GREEN value
		void setBlue(int blue);							// Sets the BLUE value
		void setBrightness(int brightness);				// SSets the brightness of a pixel
		long getColor();								// Get the uint_32 representation of the RGB value
		bool equals(YAMC_RGB rgb);						// does a deep comparison of the rgb values in the object

	private:
		int _red;
		int _green;
		int _blue;
		int _brightness;
};