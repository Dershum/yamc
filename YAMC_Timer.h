class YAMC_Timer {
    public:
        YAMC_Timer();
        YAMC_Timer(unsigned long length);               // Creates a new timer of a given length
        void startTimer();                              // Start the timer from 0
        void startTimer(unsigned long length);          // Start the timer with the given length from 0
        void stopTimer();                               // Stops the timer
        void restartTimer();                            // Restarts the timer from when it was stopped
        bool isExpired();                               // Check to see if the timer is expired
        unsigned long getTimerStart();                  // Returns the millseconds when the timer was started
        unsigned long getTimerStop();                   // Returns the milliseconds when the timer was stopped
        void setLength(unsigned long length);           // Reset the length of the timer

    private: 
        unsigned long _length;                          // Length of the timer in milliseconds
        unsigned long _timerStart;                      // When the timer was started
        unsigned long _timerStop;                       // When the timer was stopped
        bool _isRunning;                                // If the timer is running
};

